## Bland Digital Calculator

## To implement this we decided to take the base ReactApp calculator and transform it to how we want it!

![Blandalator](logotype-primary.png "Blandalator")

## Install

`npm install`

## Usage

`npm start`
