# An Automated Testing Mindset

Welcome to the practical part of the workshop. Please make sure to complete the [Getting started](#getting-started) section before the start of the course. The rest of the sections will be explained during the workshop, so you don't need to do anything else other than the [Getting started](#getting-started) steps in preparation for the course.

# Getting started

## Installing dependencies

You will need an IDE or code editor of your choice. I will be running [Visual Studio Code](https://code.visualstudio.com/).

```
brew install visual-studio-code
```

We are going to work with a React application, so please make sure you have a recent version of NodeJS:

```
brew install node
```

You will also need Git:

```
brew install git
```

## Azure DevOps account

You also need an Azure DevOps account. Please follow [this link](https://azure.microsoft.com/en-gb/services/devops/) and create an account. It should then prompt you to create an Organisation, which is a way to group your projects, so choose a name and then create a new project:
- Enter a project name (for instance, "An Automated Testing Mindset").
- The visibility of the project should be **Private**.
- In the Advanced Options, please select:
    - **Git** as the version control.
    - Either **Basic** or **Agile** as the Work Item Process.

> Please be aware that the Azure DevOps UI changes quite frequently, so these steps may be out of date! If you struggle, please let us know in the Slack channel and we will help you setting it up.

## SSH key

The next step is to add your SSH key to your Azure DevOps account. On the left sidebar, click on **Repos**. In the **Clone to your computer** section, click on the **SSH** button. A new link called **Manage SSH Keys** will appear. Click on it to take you to a new screen.

![Manage SSH Keys](img/ssh-key-manage.png ':size=600')

Click on the button to add a new key:
- Choose a name (for instance, "AND Digital laptop").
- Paste your public SSH key (instructions below).
- Click the **Add** button.

In order to copy your public key to the clipboard, go to a terminal and type the following:

```
cat ~/.ssh/id_rsa.pub | pbcopy
```

If everything went well, you should now be able to paste your public key in the previous form, and you can skip the rest of this section and move on to [Configure git](#configure-git).

If the command failed (`cat: /Users/my-user/.ssh/id_rsa.pub: No such file or directory`), it usually means that you don't have an SSH key pair yet. You will have to generate one, and you'll have to ensure that its algorithm is RSA, as it's the only one supported by Azure DevOps. In order to do that, please enter the following command:

```
ssh-keygen -t rsa -b 4096
```

The key generator program will ask you to enter the file in which the key will be stored, as well as the passphrase. You can hit enter when prompted to accept the default options (save the key in `~/.ssh/id_rsa` and no passphrase).

When you run the command to copy the public key to the clipboard, it should now succeed without returning an error. Paste it in the web form, and click **Add** to finish configuring your SSH key in Azure DevOps.

## Configure git

Go to your terminal, create a new folder for the workshop and cd into it:

```
mkdir ~/an-automated-testing-mindset && cd "$_"
```

Clone the workshop's repository:

```
git clone https://gitlab.com/jmgq/blandinator.git
```

Cd into it:

```
cd blandinator
```

Ensure that git is configured with the same email as the one you used when you created your Azure DevOps account. In order to display your git email, please type:

```
git config user.email
```

If the email is different, you can change it with the same command, followed by your email (e.g. `git config user.email john.doe@and.digital`).

You can also configure your git name in the same fashion (that is, `git config user.name` to display and `git.config user.name "John Doe"` to change it).

The next step is to replace the `origin` remote repository, from GitLab to Azure DevOps.
1. To remove the existing origin, type:
    ```
    git remote remove origin
    ```
2. To add the new origin, in Azure DevOps, navigate back to **Repos**. In the **Push an existing repository from command line** section, click the **SSH** button. Copy the whole text box by clicking on the copy button to its right. Please use this button and do not attempt to manually copy the text, as it contains two commands (`git remote add` and `git push`) in two lines and it is easy to miss the second one. Paste it in your terminal. If you refresh the **Repos** page, it should now display our workshop codebase.

## Setup completed

Thank you for completing the setup. This is all you have to do for now. During the workshop, we will build a pipeline together, in an interactive way. The rest of this guide is here merely to help you get back on track if you struggle with some of the steps.

# Creating a basic pipeline

## Template

In the **Pipelines** section, click on the **Create Pipeline** button. Next, click on the link at the bottom that says **Use the classic editor to create a pipeline without YAML**.

![Classic editor](img/template-classic-editor.png ':size=400')

The source of your repository should be the default: Azure Repos Git. In the next screen, use the search function to select a template called **Node.js With gulp**. Click on **Apply**.

This prebuilt pipeline has 4 steps:
1. npm install.
2. Run gulp task.
3. Archive files.
4. Publish artifacts: drop.

Remove all of them, except **npm install**. There are different ways to remove them. For instance, you can right click on them and select **Remove selected task(s)**, or select the tasks individually and click **🗑️ Remove** at the top right.

At the top of the list of tasks, there is a section called **Pipeline**. Click on it. In **Agent Pool**, select **Default**.

![Pipeline configuration](img/template-pipeline.png ':size=800')

In the top bar, the **Save & queue** button should now be enabled: click on it and, in the dropdown, select **Save & queue**. In the next popup, click on **Save and run**. It will fail, because we haven't set up an agent yet.

## Local agent

### Create a personal access token

Go to **User Settings** (in the top right corner, the second button from the right) and select **Personal access tokens** in the dropdown.

![Personal access tokens](img/local-agent-personal-access-tokens.png ':size=400')

Click on the **New token** button, give it a name and ensure that the **Scope** is set to **Full access**. Click on **Create**. This will generate a new token. Make sure that you **copy** it, because once you close that page, you will not be able to get it again. If you lose the token, you will have to create a new one.

### Install the agent

The next step is to install a new agent in your laptop. But first we need to change a security setting to allow you to run apps downloaded from anywhere. In order to do so, please run the following command:

```
sudo spctl --master-disable
```

!> When this workshop is over, remember to revert this by running `sudo spctl --master-enable`.

Navigate to your organisation settings by clicking on the Azure DevOps logo on the top left of your screen, and then on **Organization settings** at the bottom left.

![Organization settings](img/organization-settings.png ':size=200')

In the left menu, under **Pipelines**, select **Agent pools**, and then click on **Default**. In the **Agents** tab, there will be an option to create a **New agent**. A new popup will appear, click on the **Download** button. Run the following two commands to decompress the file you just downloaded:

```
mkdir -p ~/an-automated-testing-mindset/agent
tar zxvf ~/Downloads/vsts-* --directory ~/an-automated-testing-mindset/agent
```

### Configure the agent

Run the configuration script:

```
cd ~/an-automated-testing-mindset/agent
./config.sh
```

It will ask you multiple questions, you need to answer the following:
1. Enter **Y** to accept the license agreement.
2. Enter the sever URL, in the following form: `https://dev.azure.com/your-organization`. Your organisation is the name you chose when you registered in Azure DevOps. An easy way to get this is to simply go to your browser and copy it from the address bar.
3. Hit enter to choose the default authentication type (PAT).
4. Paste your personal authentication token that you obtained in the previous [Create a personal access token](#create-a-personal-access-token) section.
5. Hit enter again to select the default agent pool.
6. Choose a name for your agent (by default it is your laptop's hostname).
7. Hit enter again to accept the default work folder.

### Run the agent

```
cd ~/an-automated-testing-mindset/agent
./run.sh
```

Remember that if you stop the agent (for instance, by restarting your laptop), you will have to execute the `run.sh` script again.

## Running the tests in the pipeline

Edit your pipeline and add a new step by clicking the plus sign (➕) next to you Agent Job and searching for **npm**.

![Add new generic npm task](img/add-new-generic-npm-task.png)

By default, the new task will be **npm install**, so you should now have two identical tasks. Go ahead and edit the second one, with the following information:
1. Command: **custom**
2. Display name: **Build**
3. Command and arguments: **run build**

Add a third task, this time by right clicking the **npm build** task and selecting **Clone task(s)**. Edit the new task:
1. Display name: **Test suite**
2. Command and arguments: **run test**

Save & queue. The pipeline will fail, as the build is broken.

## Fix the build

<details>
    <summary></summary>

Edit your `.eslintrc.js` file and change the line break style from `windows` to `unix`.

</details>

Commit and push your changes. Make sure to add the task number to your commit message. For instance:

```
git commit -m "#1 Fix the build"
```

By default, the pipeline does not run automatically with new commits. To change this behaviour, edit your pipeline, select the **Triggers** tab and mark the **Enable continuous integration** option. Remember to save the changes (in the **Save & queue** dropdown).

# Development workflow improvements

Install the following Visual Studio Code plugins:
1. ESLint.
2. Jest.
3. Prettier - Code formatter.

Configure Prettier by pressing **Command + Shift + P**, typing **settings.json**, selecting **Preferences: Open Settings (JSON)** and adding the following lines:

```javascript
  "editor.formatOnSave": true,
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
```

We can leverage ESLint to introduce Static Application Security Testing (SAST), in order to scan for known vulnerabilities. In `.eslintrc.js`:
1. Add `"security"` to the `"plugins"` section.
2. Add `"plugin:security/recommended"` to the `"extends"` section.

Husky is already installed, so we can configure it to enable pre-commit hooks, by adding the following lines to `package.json`:

```javascript
"husky": {
  "hooks": {
    "pre-commit": "npm run lintfix",
    "pre-push": "npm run test"
  }
}
```

# Fixing the tests

When you run `npm test`, the test suite does not pass.

<details>
    <summary></summary>

1. `src/logic/isNumber.test.js`, in line 4, change `"a"` to `"1"`:
    ```javascript
    expect(isNumber("1")).toBe(true);
    ```
2. `src/index.integration.test.js`, in line 38, change `"2"` to `"3"`:
    ```javascript
    expect(componentDisplay.innerHTML).toBe("3");
    ```

</details>

What improvements can you think of?

<details>
    <summary></summary>

1. `jest.spyOn(window, "fetch");` is not necessary.
2. `beforeEach`, for instance:
    ```javascript
    let componentDisplay;

    beforeEach(() => {
        const dom = render(<App />);

        componentDisplay = dom.container.getElementsByClassName(
            "component-display",
        )[0].children[0];
    });
    ```
3. Parametrised tests, using [test.each](https://jestjs.io/docs/api#testeachtablename-fn-timeout).
4. Keep the tests simple.
5. More descriptive test names, for instance `it("should add two numbers")`.

</details>

# Separating the tests

How would you separate the unit and the integration tests?

<details>
    <summary></summary>

In `package.json`, in the `"scripts"` section, add the following:

```javascript
"test-unit": "react-scripts test --env=jsdom --watchAll=false --testPathIgnorePatterns=integration",
"test-integration": "react-scripts test --env=jsdom --watchAll=false integration",
```

</details>

How would you run them in the pipeline?

<details>
    <summary></summary>

We need two npm tasks, both of them will have their **Command** parameter set to **Custom**, and their **Command and arguments** parameter set to `run test-unit` and `run test-integration`.

</details>

How would you improve the pipeline?

<details>
    <summary></summary>

1. Adding ESLint.
2. Reordering the tasks: npm install > eslint > unit tests > integration tests > build.

</details>

# Cache

In order to cache the `node_modules` folder, we need to add a task called **Cache** at the very top of our list of tasks, with the following parameters:
1. Key: `node_modules | $(Agent.OS) | package-lock.json`
2. Path: `./node_modules`

The first time to run will cause a cache miss, but in subsequent runs there will be a cache hit. However, it still has to run `npm install`, which takes too long. The solution is to skip `npm install` when there is a cache hit:
1. Update the **Cache** task and enter `CACHE_RESTORED` in the **Cache hit variable** section.
    ![Cache task](img/cache-task.png)
2. Update the **npm install** task: in **Control options** there is a section called **Run this task**. Select **Custom conditions** in the dropdown. Enter `ne(variables.CACHE_RESTORED, 'true')` in the **Custom condition** field.
    ![Cache npm install](img/cache-npm-install.png)

# Displaying the test results

<details>
    <summary></summary>

1. Install the `jest-junit` library by running `npm install --save-dev jest-junit`.
2. Since we have two test scripts (`test-unit` and `test-integration`), `junit.xml` will be overridden when we run the second test script. We can use the `JEST_JUNIT_OUTPUT_NAME` environment variable to change the xml file name. An additional issue is that after adding the `jest-junit` reporter, the test results will no longer be printed in the console. To fix this, we need to explicitly add both reporters: `--reporters=default --reporters=jest-junit`. In summary, both test scripts (located in `package.json`) should look like this:
    ```javascript
    "test-unit": "JEST_JUNIT_OUTPUT_NAME=junit-unit.xml react-scripts test --env=jsdom --watchAll=false --testPathIgnorePatterns=integration --reporters=default --reporters=jest-junit",
    "test-integration": "JEST_JUNIT_OUTPUT_NAME=junit-integration.xml react-scripts test --env=jsdom --watchAll=false integration --reporters=default --reporters=jest-junit",
    ```
3. Add `junit-*.xml` to `.gitignore`.
4. Search for the **Publish Test Results** task in your pipeline, and add it after the task that runs your integration tests. Make sure that the **Test result format** is **JUnit** and that the **Test results files** is `**/junit-*.xml`.

</details>

# Release

## Hello world release

Add a new task at the bottom of your pipeline called **Publish build artifact**. In both the **Path to publish** and **Artifact name**, enter `build`.

In the left side menu, click on **Releases** (under **Pipelines**), and add click on the button to add a new pipeline. This type of pipeline is different to your existing pipeline. In the new popup, do not select a template; instead, click on **Empty job**.

![New release with empty job](img/release-empty-job.png)

Change the name of the pipeline from the default **New release pipeline** to **Release to Prod**. Click on the existing **Stage 1** box and change its name to **Ship code**.

Click on the **Tasks** tab, select **Agent job** and choose the **Default** option in the **Agent pool** dropdown.

![Agent configuration](img/release-agent.png ':size=800')

Add a new task by clicking on the plus sign (➕) and searching for **Bash**. Ensure that the **Type** is **Inline** and enter the following in the **Script** textbox:

```
echo "Listing artifacts..."
find . -print
```

It should look like this:

![Task configuration](img/release-task.png ':size=800')

Click on the **Pipeline** tab and add a new artifact. The **Source alias** should be `blandinator-artifacts`. Click on the **Add** button.

![New artifact](img/release-artifact.png ':size=800')

Click on the lightning icon (⚡) of your artifact and enable the **Continuous deployment trigger** option:

![Continuous deployment](img/release-continuous-deployment.png ':size=800')

Click on the **Save** button on the top right of your screen to save the new release pipeline.

## Real release

Since the calculator is a web application, we can release it online using a free deployment service like [Surge](https://surge.sh/).

<details>
    <summary></summary>

Install surge:

```
npm install --global surge
```

Go to your project's build folder:

```
cd ~/an-automated-testing-mindset/blandinator/build
```

Run surge. If this is the first time you use surge, it will try to create a new account. It will also ask you to create a new domain. Make sure to remember both the email address and the domain you entered, as you will need them later. This will also deploy your project:

```
surge
```

Generate a token:

```
surge token
```

Your release pipeline should consist of two tasks:
1. An **npm** task with the following configuration:
    - **Display name**: Install Surge.
    - **Command**: custom.
    - **Command and arguments**: `install surge`.
2. A **Bash** task with the following configuration:
    - **Display name**: Upload to Surge.
    - **Type**: Inline.
    - **Script**:
        ```
        npx surge --project blandinator-artifacts/build --domain your-domain.surge.sh
        ```
        > Make sure to change `your-domain.surge.sh` to the domain you entered in the previous step.
    - **Environment variables**:
        | Name          | Value                                                         |
        |---------------|---------------------------------------------------------------|
        | `SURGE_LOGIN` | The email you used to register in surge                       |
        | `SURGE_TOKEN` | The token you generated previously when you ran `surge token` |

</details>

# The end

Congratulations for completing the workshop. We hope you have enjoyed it and that you have found it useful. Please remember to revert the previous System Policy changes by running:

```
sudo spctl --master-enable
```
